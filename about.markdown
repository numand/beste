---
layout: about
---

## About

Hello there. 

## Contact

You can reach me  at [e-mail] . If you want to be secretive, use my <a href="/assets/numandemirdogen.asc">PGP key</a> .

## Projects

Here are the projects 

### `Beste`

A Jekyll theme.

* Project Source Code: [https://gitlab.com/numand/numand.gitlab.io]
* License: [GPLv3]

## Colophon

The content of this site (articles, drawings, etc.) is licensed under the [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)] license, unless otherwise stated.

### `Design`

The site's design is based on the [theme] Ruud van Asseldonk used for his site. Colors from Ethan Schoonover's [Solarized] palette are used throughout the site.

### `Technology`

This site is served via the [Jekyll] static site generator using the [Gitlab Pages] infrastructure.

[e-mail]: mailto:if.gnu.linux@gmail.com
[GPLv3]: https://www.gnu.org/licenses/gpl-3.0.html
[https://gitlab.com/numand/numand.gitlab.io]: https://gitlab.com/numand/numand.gitlab.io
[Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)]: https://creativecommons.org/licenses/by-sa/4.0/
[theme]: https://github.com/ruuda/blog
[Solarized]: http://ethanschoonover.com/solarized
[Gitlab Pages]: https://pages.gitlab.io/
[Jekyll]: https://jekyllrb.com/
