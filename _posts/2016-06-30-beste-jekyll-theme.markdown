---
layout : post
title : "Beste"
subtitle : "A Minimalist Jekyll Theme"
summary : "How I Ported a Hakyll Theme to Jekyll "
date : 2016-06-30 10:32:13 +0300
language : "english"
math: "yes"
categories: 
- software
- jekyll
tags:
- jekyll theme
---

<span class="run-in">As a person</span> who helps users of GNU/Linux distributions on local forum and translates some applications to my mother tongue, I write lots of how-tos and notes related to what I do. For a while I have been thinking that to publish these how-tos and notes in the hope that they are helpful to people. Thanks to the [Gitlab Pages], I can achieve what I wanted to do. As you can read from Gitlab Pages, Gitlab provides a website for Gitlab projects, groups or a user account. You can use most of the [static site generators], such as Jekyll, Pelican and Hexo and even [example websites] are provided for a starting point.

There are two criteria I seek from a static site generator: it must have an esthetically pleasing theme and be easy to configure. For me, an esthetically pleasing theme must be easy to eyes and simplistic in order to make reader focus to text. Therefore I searched over themes provided by static site generators. At last I choose a Hakyll [theme] written by Ruud van Asseldonk which is exactly looks like what I want. But the problem of using Hakyll is that you need to download hundreds of megabytes of packages in order to use a simple theme and know Haskell to change a theme. Well, for a person who knows nothing about programming languages such as myself, it is a long shot. That is to say, Hakyll could not meet my second criteria. When I thought that all hope for using that theme was lost, I learned that Jekyll can serve HTML pages. Since Ruud's blog is consisted of HTML pages. I can use these pages with Jekyll. So I began to port the theme to Jekyll.

## Copying essential files

First thing to do is to determine how to use HTML files in Ruud's blog for a Jekyll theme. According to Jekyll documentation, a basic Jekyll site usually looks something like this:

{% highlight bash %}
├── about.html
├── _config.yml
├── _drafts
│   └── something.markdown
├── feed.xml
├── _includes
│   ├── footer.html
│   ├── head.html
│   ├── page.css
├── index.html
├── _layouts
│   ├── default.html
│   └── post.html
├── _posts
│   ├── something.markdown
└── _site
{% endhighlight %}

This means that I need an `index.html` file for home page, a `_layouts` directory which contains files likes `post.html` for positioning elements in posts, and an `_includes` directory for completing layouts and implementing CSS. According to this, I placed relative files as shown in below:

* templates/index.html &rarr; about.html
* templates/contact.html &rarr; contact.html
* templates/archive.html &rarr; index.html
* templates/post.html &rarr; _layouts/post.html
* templates/footer.html &rarr; _includes/footer.html
* templates/head.html &rarr; _includes/head.html
* templates/page.css &rarr; _includes/page.css
* templates/fonts.css &rarr; _includes/fonts.css
* templates/math.css &rarr; _includes/math.css

I decided to merge `about.hmtl` and `contact.html` pages in one page and turned in to a Markdown one because I wanted to write not only about me and a contact information but also projects that belongs to me and I am involved, and a colophon. A Markdown page will give me much more flexibility for what I want. 

So I will have three pages for my site: a home page, an about page and a page for posts. 

## Deciding Layout

Ruud uses Hakyll to parse these files in order to get what he wants so these files need to be modified to be used in Jekyll. But before this, I had to create layouts for each page (home, about and post) in the site. Let's look at the structure of each page:

#### About Page Layout:
![about_layout][about_layout]

#### Home Page Layout:
![index_layout][index_layout]

#### Post Page Layout:
![post_layout][post_layout]

`_layouts/about.html` will be used for about page (`about.markdown`).

`index.html` will be my home page and it will have a layout named `page.html` in `_layouts` directory.

And `post.html` file will be used for posts as a layout.

## Porting to Jekyll

### _layouts/about.html

I started porting by writing `about.html` layout since it is the most easy one. All I needed to do was change links and remove `<p>` element and its content from index.html file and add `{% raw %}{{ content }}{% endraw %}` tag instead.

### _layouts/page.html

For this, all I needed to include `head.html` and `footer.html` files and add `{% raw %}{{ content }}{% endraw %}` tag.

{% highlight liquid %}
{% raw %}
{% include head.html %}
<body>
{{ content }}
{% include footer.html %}
</body>
</html>
{% endraw %}
{% endhighlight %}

You may wonder where the opening of `html` tag is. As you know, Jekyll merges all these layouts and relatives files into a single static HTML file. So some part of a HTML tag can be placed in say a.html file whereas other part of it can be placed in b.html file. That's why opening of `html` tag is in the `head.html` file.

### _layouts/post.html

Header of this layout must contain title and subtitle of the writing, author's name, date of the post. If writing consists of multiple parts, part of the writing will be shown. For this, I created a user defined **Liquid tag**: `part`.

{% highlight liquid %}
{% raw %}
{% if page.part %}
  <p class="smcp">{{ page.part }}</p>
{% endif %}
{% endraw %}
{% endhighlight %}

If I write in English, I can put 

{% highlight liquid %}
{% raw %}
{{ page.date | date: "%-d %B, %Y" }}
{% endraw %}
{% endhighlight %}

and move on but when I write in Turkish, date must be displayed in my language. To get that, a little bit of Liqud code was necessary. First, I need a variable indicating that whether a post is English or not. So, a `language` Liquid tag is defined. Then I must turn English month names into Turkish ones.

{% highlight liquid %}
{% raw %}
{% if page.language == 'english' %}
  {{ page.date | date: "%-d %B, %Y" }}
{% else %}
  {{ page.date | date: "%-d" }}
  {% assign m = page.date | date: "%B" %}
  {% case m %}
    {% when 'January' %}Ocak,
    {% when 'February' %}Şubat,
    {% when 'March' %}Mart,
    {% when 'April' %}Nisan,
    {% when 'May' %}Mayıs,
    {% when 'June' %}Haziran,
    {% when 'July' %}Temmuz,
    {% when 'August' %}Ağustos,
    {% when 'September' %}Eylül,
    {% when 'October' %}Ekim,
    {% when 'November' %}Kasım,
    {% when 'December' %}Aralık,
  {% endcase %}
  {{ page.date | date: "%Y" }}
{% endif %}
{% endraw %}
{% endhighlight %}

If the post is not in English, I assign English names of month to a variable called `m`; then put the Turkish equivalent if `m` matches any of mount name. 

### index.html

First challenge was to put a *year* indicating year of posts groups in which they were written. Simply putting a `{% raw %}{%  post.date %}{% endraw %}` resulted in *year* showing next to every post title. I could not find an answer to this one  by myself but an [answer] from a Stackoverflow user named Christian Specht rescued me. I tweaked the Liquid code a bit and achieved my goal.
{% highlight liquid %}
{% raw %}
{% for post in site.posts %}
  {% assign currentdate = post.date | date: "%Y" %}
  {% if currentdate != date %}
    {% unless forloop.first %}{% endunless %}
    <h2>{{ currentdate }}</h2>
    {% assign date = currentdate %}
  {% endif %}
  <h3><a href="{{ post.url }}">{{ post.title }}
  {% if post.subtitle %}: {{ post.subtitle }}{% endif %}
  </a></h3>
  {% if forloop.last %}{% endif %}
{% endfor %}
{% endraw %}
{% endhighlight %}

This code assigns year of a post to `currentdate` variable then it compares `currentdate` to current year. If `currentdate` is different than current year, it prints the year of post until it reaches current year. Well, that is what I understand from it:).

Next challenge was to print a summary of post under post title. Ruud uses a user defined variable, `synopsis`, in a post and `{% raw %}{{ synopsis-html }}{% endraw %}` tag in `archieve.html` file. Jekyll uses `page.excerpt` tag to give a hint about post's content but all you can do is to include a couple of sentence of first paragraph or first paragraph itself. It is not a flexible way. Instead of this, I created a Liquid tag named `summary` and added

{% highlight liquid %}
{% raw %}
{% if post.summary %}
  <p>{{ post.summary | strip_html }}
  {% if post.language == 'english' %}
    <a href="{{ post.url }}" class="smcp">READ POST</a></p>
  {% else %}
    <a href="{{ post.url }}" class="smcp">YAZIYI OKU</a></p>
  {% endif %}
{% endif %}
{% endraw %}
{% endhighlight %}

lines after closing `<h3>` tag and before closing `forloop`.

### _includes/head.html

If a post contains mathemetical formulas, `_includes/math.css` should be includes in main CSS file. In order to achieve this, Ruud uses `{% raw %}{{math}}{% endraw %}` tag. For Jekyll, I created a similar tag and added

{% highlight liquid %}
{% raw %}
  {% if page.math == 'yes' %}{% include math.css %}{% endif %}
{% endraw %}
{% endhighlight %}

line to `head.html` file.

### _includes/teaser.html

Teaser section of Ruud's site is included in `post.html` file and it is a relatively simple one. 

{% highlight html %}
<section id="teaser-section">
<div id="teaser">
  <p class="smcp">More words</p>
  <h2>{% raw %}{{further.title}}{% endraw %}</h2>
  <p>{% raw %}{{further.synopsis-html}}{% endraw %} <a href="{{further.url}}" class="smcp">Read full post</a></p>
</div>
</section>
{% endhighlight %}

Since I will write posts both in Turkish and English and have to use Liquid tags that I created, teaser section had to be re-written. Additionally, I decided to add relative information about previous posts if reader is reading the last post in my site. 

"More words" will be replaced with "next/sonraki" or "previous/önceki" according to `language` tag and whether the post is the last one or nor.

`{% raw %}{{further.title}}{% endraw %}` tag will be replaced with equivalent Liqud tags: `{% raw %}{{ page.next.title }}{% endraw %}` or `{% raw %}{{ page.previous.title }}{% endraw %}` and if the next or previous post has a subtitle, it will be included with `{% raw %}{{ page.next.subtitle }}{% endraw %}` or `{% raw %}{{ page.previous.subtitle }}{% endraw %}`

### _includes/footer.html

There is nothing important to mention about changes in this file.

## CSS

In order to use Ruud's `page.css` file, I needed to remove some Haskell codes(?) and apply color from Solarized color palette.

### _includes/page.css

To use some font's Open Type features like kerning, ligaratures and number lining, I needed to declare some CSS properties different from how Ruud used.

For kerning, Ruud uses 

{% highlight css %}
font-feature-settings: 'kern';
{% endhighlight %}

CSS property. But according to [CSS Fonts Module Level 3], [font-kerning] property is enough.

{% highlight css %}
font-kerning: normal;
{% endhighlight %}

To use ligaratures, number lining, slashed zero and small caps, instead of using font-features-settings, it is [advised] to use shorthand or longhand property:

> Whenever possible, Web authors should use the font-variant shorthand property or an associated longhand property, font-variant-ligatures, font-variant-caps, font-variant-east-asian, font-variant-alternates, font-variant-numeric or font-variant-position.
> 
> This property is a low-level feature designed to handle special cases where no other way to enable or access an OpenType font feature exists.
> 
> In particular, this CSS property shouldn't be used to enable small caps.

That's why I use font-variant property:

{% highlight css %}
font-variant-ligatures: common-ligatures;
font-variant-numeric: lining-nums;
font-variant-ligatures: discretionary-ligatures;
font-variant-numeric: slashed-zero;
font-variant-caps: small-caps;
{% endhighlight %}

[Gitlab Pages]: https://about.gitlab.com/features/pages
[static site generators]: https://www.staticgen.com
[example websites]: https://gitlab.com/groups/pages
[theme]: https://github.com/ruuda/blog
[about_layout]: /assets/images/about_layout.svg
[index_layout]: /assets/images/index_layout.svg
[post_layout]: /assets/images/post_layout.svg
[answer]: https://stackoverflow.com/questions/19086284/jekyll-liquid-templating-how-to-group-blog-posts-by-year/20777475#20777475
[CSS Fonts Module Level 3]: https://drafts.csswg.org/css-fonts-3
[font-kerning]: https://drafts.csswg.org/css-fonts-3/#propdef-font-kerning
[advised]: https://developer.mozilla.org/en-US/docs/Web/CSS/font-feature-settings