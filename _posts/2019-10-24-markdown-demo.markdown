---
layout: post
title: "Markdown Demo"
author: Mr. Yellow
subtitle: "Examples of Markdown Syntax"
summary: "Demonstrating how Markdown syntax looks like on the site "
date: 2020-10-24
math: "yes"
language: "english"
categories: 
- jekyll
tags:
- markdown
---

This is Markdown cheat sheet demo for **Beste**, this Jekyll theme. Please check the raw content of this file for the markdown usage.

## Headers

Headers will look like this:

# Text in H1

## Text in H2

### Text in H3

For second level headers, serif font is used.

## Blockquotes

> Blockquotes will be indented
> and serif font will be used for them.

Quote break.

> This is a very long line that will still be quoted properly when it wraps. For long line like this, you only need to place > before the first line of a wrapped text. Oh, you can *use* **Markdown** in a blockquote.

Quote break.

> Also, blockquotes can be nested
>
> > by adding >'s.

## Lists

1. First ordered list item
2. Another item
1. Actual numbers don't matter, just that it's a number
    - Ordered sub-list
    - Second one
4. And another item.

* Unordered lists that use asterisks
- Or minuses
+ Or pluses
* will be shown with a dot.

1.  This is a list item with two paragraphs. The quick,
     brown fox jumps over a lazy dog. DJs flock by
     when MTV ax quiz prog.

    Junk MTV quiz graced by fox whelps. Bawds jog,
    flick quartz, vex nymphs. Waltz, bad nymph, for
    quick jigs vex! Fox nymphs grab quick-jived waltz.

2.  Brick quiz whangs jumpy veldt fox. Bright vixens jump;
     dozy fowl quack.

*   A list item with a blockquote:

    > This is a blockquote
    > inside a list item.

*   A list item with a code block:

        {% highlight bash %}if [[ "${HOME}" == "/home/ubuntu" ]] ; then printf "%s" "OK"; fi{% endhighlight %}

## Horizontal Rules

Three or more...

Hyphens

---

Asterisks

***

Underscores

___

## Links

Links will be underlined when hovered.

[Here's an example](https://numand.gitlab.io)

## Footnotes

Everyone loves footnotes[^1]

## Emphasis

Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~

## Sub and Super Scripts

5<sup>3</sup> = 125. Water is H<sub>2</sub>O.

## Code and Syntax Highlighting 

Jekyll uses the [Rouge Ruby library][rouge] for syntax highlighting. For a list of supported languages visit the Rouge website.

For syntax highlighting, use
{% raw %}
{% highligh name_of_programming_language linenos %}
	code
{% endhighlight %}
{% endraw %}
[Liquid] tag.

This sentence has an `inline code` in it. All codes will use DejaVu Sans Mono font and light color scheme of Solarized color palette. Here are some examples:

{% highlight python %}
#### Python 3: Fibonacci series up to n
def fib(n):
    a, b = 0, 1
    while a < n:
        print(a, end=' ')
        a, b = b, a+b
    print()
    fib(1000)
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987
{% endhighlight %}

{% highlight ruby linenos %}
#### Ruby: Say hi to everybody
def say_hi
  if @names.nil?
    puts "..."
  elsif @names.respond_to?("each")

    #### @names is a list of some kind, iterate!
​    @names.each do |name|
​      puts "Hello #{name}!"
​    end
  else
​    puts "Hello #{@names}!"
  end
end
{% endhighlight %}

## Math 

This theme uses Katex for drawing mathematic expressions. 

Euler's formula is remarkable: $$ e^{i\pi} + 1 = 0 $$ .

$$
\begin{aligned}
  & \phi(x,y) = \phi \left(\sum_{i=1}^n x_ie_i, \sum_{j=1}^n y_je_j \right)
  = \sum_{i=1}^n \sum_{j=1}^n x_i y_j \phi(e_i, e_j) = \\
  & (x_1, \ldots, x_n) \left( \begin{array}{ccc}
      \phi(e_1, e_1) & \cdots & \phi(e_1, e_n) \\
      \vdots & \ddots & \vdots \\
      \phi(e_n, e_1) & \cdots & \phi(e_n, e_n)
    \end{array} \right)
  \left( \begin{array}{c}
      y_1 \\
      \vdots \\
      y_n
    \end{array} \right)
\end{aligned}
$$

## Images

Here's this site's favicon:

![favicon](/assets/favicon.jpg){: #boxshadow}

Images will be scaled according to screen size.

![ubuntu][ubuntu]{: #boxshadow}

## Tables

| header 1 | header 2 |
| -------- | -------- |
| cell 1   | cell 2   |
| cell 3   | cell 4   |

**Note**

The row of dashes between the table header and body must have at least three dashes in each column.

By including colons in the header row, you can align the text within that column:

| Left Aligned | Centered | Right Aligned | Left Aligned | Centered | Right Aligned |
| :----------- | :------: | ------------: | :----------- | :------: | ------------: |
| Cell 1       | Cell 2   | Cell 3        | Cell 4       | Cell 5   | Cell 6        |
| Cell 7       | Cell 8   | Cell 9        | Cell 10      | Cell 11  | Cell 12       |

## More About Markdown

If you are new to Markdown syntax, here is an exellent [source] from John Gruber, who wrote Markdown.

[rouge]: http://rouge.jneen.net/
[Liquid]: https://shopify.github.io/liquid/
[ubuntu]: /assets/images/canonical-friends_orange_hex.svg "Ubuntu logo"
[^1]: Let's put the explanation of footnote in here.
[source]: https://daringfireball.net/projects/markdown/syntax