[![pipeline status](https://gitlab.com/numand/beste/badges/master/pipeline.svg)](https://gitlab.com/numand/beste/-/commits/master)7


[![coverage report](https://gitlab.com/numand/beste/badges/master/coverage.svg)](https://gitlab.com/numand/beste/-/commits/master) 

# Beste

Beste is a Jekyll theme that tries to be minimal and aesthetically pleasing by using typography and warm colour palette. This theme is a fork of this [theme]. You can read how I portred Ruud's theme to Jekyll in [here].

You can see theme in live: https://numand.gitlab.io

## How to use it

TODO

## How to contribute

There is no need a contribution guide. I am not a programmer. I do not know CSS, HTML and Liquid. While making this theme, I read a lot of documentations about CSS, HTML, Liquid and Jekyll in order to make theme work. But, I probably make a lot of mistakes. I would be very grateful if you point my mistakes. 

## License

This theme is distributed under GNU General Public License Version 3 ([GPLv3]).

[theme]: https://github.com/ruuda/blog
[here]: https://numand.gitlab.io/linux/jekyll/2016/06/30/beste-jekyll-theme.html
[GPLv3]: https://www.gnu.org/licenses/gpl-3.0.html